#include <stdlib.h>
#include <stdio.h>

#include <gpg-error.h>

int main()
{
    const char* version = gpg_error_check_version(GPG_ERROR_VERSION);
    if (version == NULL) {
        perror("Could not check 'gpg error' version");
        return EXIT_FAILURE;
    }
    printf("GPG Error version: %s\n", version);
    return EXIT_SUCCESS;
}
