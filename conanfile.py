#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from conans import ConanFile, AutoToolsBuildEnvironment, tools


class LibGpgErrorConan(ConanFile):
    name = "libgpg-error"
    version = "1.33"
    description = "A library that originally defined common error values for all GnuPG components"
    topics = ("conan", "libgpg-error", "gpg", "gpg-error", "gcrypt")
    url = "https://github.com/bincrafters/conan-libgpg-error"
    homepage = "https://www.gnupg.org/software/libgpg-error/index.html"
    author = "Bincrafters <bincrafters@gmail.com>"
    license = "GPL-2.0"
    exports = ["LICENSE.md"]
    settings = "os", "arch", "compiler", "build_type"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    _source_subfolder = "source_subfolder"
    _autotools = None

    def config_options(self):
        if self.settings.os == 'Windows':
            del self.options.fPIC

    def configure(self):
        del self.settings.compiler.libcxx

    def source(self):
        source_url = "https://www.gnupg.org/ftp/gcrypt/libgpg-error"
        sha256 = "0841460cfc2e0324dda83bcca6910072bbc980176ae5ed13ab17265331c3c49a"
        tools.get("{}/{}-{}.tar.gz".format(source_url, self.name, self.version), sha256=sha256)
        extracted_dir = self.name + "-" + self.version
        os.rename(extracted_dir, self._source_subfolder)

    def _configure_autotools(self):
        if not self._autotools:
            self._autotools = AutoToolsBuildEnvironment(self, win_bash=tools.os_info.is_windows)
            configure_args = [
                "--disable-doc",
                "--disable-tests",
                "--disable-languages",
                "--enable-shared=%s" % ("yes" if self.options.shared else "no"),
                "--enable-static=%s" % ("no" if self.options.shared else "yes"),
                "--with-fpic=no"
            ]
            self._autotools.configure(args=configure_args)
        return self._autotools

    def build(self):
        with tools.chdir(self._source_subfolder):
            autotools = self._configure_autotools()
            autotools.make()

    def package(self):
        self.copy(pattern="COPYING*", dst="licenses", src=self._source_subfolder)
        with tools.chdir(self._source_subfolder):
            autotools = self._configure_autotools()
            autotools.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
